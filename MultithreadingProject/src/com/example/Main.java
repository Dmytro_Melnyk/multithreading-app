package com.example;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            new Multithreading().startCommunicate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
